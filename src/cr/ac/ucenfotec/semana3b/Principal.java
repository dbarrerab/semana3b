package cr.ac.ucenfotec.semana3b;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Principal {

    //atributos
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Empleado[] empleados = new Empleado[10];

    public static void main(String[] args) throws IOException {
        menu();
    }

    private static void menu() throws IOException {
        int opcion = 0;
        do {
            System.out.println("*** Bienvenido al sistema");
            System.out.println("1. Registrar empleado");
            System.out.println("2. Listar empleados");
            System.out.println("3. Salir.");
            System.out.println("Digite una opcion");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion (opcion);
        }while (opcion != 3);
    }

    private static void procesarOpcion(int opcion) throws IOException {
        switch (opcion){
            case 1: registrarEmpleado();
                break;
            case 2: listarEmpleados();
                break;
            case 3:
                System.out.println("Gracias, vuelva pronto");
                System.exit(0);
                break; //opcional ya que es la opcion para salir
            default:
                System.out.println("Opcion invalida.");
        }
    }



    private static void registrarEmpleado() throws IOException {
        System.out.print("Digite el nombre del empleado:");
        String inCedula = in.readLine();
        System.out.print("Digite el numero de cedula:");
        String inNombre = in.readLine();
        System.out.print("Digite el puesoto del empleado");
        String inPuesto = in.readLine();

        Empleado empleado = new Empleado (inCedula, inNombre, inPuesto);

        for (int x = 0; x< empleados.length; x++) {
            if (empleados[x] != null) {
                empleados[x] = empleado;
                break;
            }
        }
    }

    static void imprimir(){
        for(int i = 0;i<empleados.length;i++){

            out.println("el nombre del empleado es" + empleados[i].nombre);
        }
    }

    }
